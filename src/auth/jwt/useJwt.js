import useJwt from '@core/auth/jwt/useJwt'
import axios from '@/libs/axios'

const { jwt } = useJwt(axios, {
  loginEndpoint: '/auth/login',
  registerEndpoint: '/auth/register',
  refreshEndpoint: '/auth/refresh',
  logoutEndpoint: '/auth/logout',
})
export default jwt
