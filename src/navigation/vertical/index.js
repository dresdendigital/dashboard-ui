export default [
  {
    title: 'Average Order Value',
    route: 'aov',
    icon: 'DollarSignIcon',
  },
  {
    title: 'Consultation',
    route: 'consultation',
    icon: 'EyeIcon',
  },
  {
    title: 'Conversion',
    route: 'conversion',
    icon: 'PieChartIcon',
  },
]
