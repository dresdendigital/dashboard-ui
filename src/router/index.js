import Vue from 'vue'
import VueRouter from 'vue-router'
import { isUserLoggedIn } from '@/auth/utils'

Vue.use(VueRouter)

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  scrollBehavior() {
    return { x: 0, y: 0 }
  },
  routes: [
    {
      path: '/',
      name: 'home',
      redirect: '/aov',
      component: () => import('@/views/Home.vue'),
      meta: {
        pageTitle: 'Home',
        breadcrumb: [
          {
            text: 'Home',
            active: true,
          },
        ],
      },
    },
    {
      path: '/AOV',
      name: 'aov',
      component: () => import('@/views/AOV.vue'),
      meta: {
        pageTitle: 'Average Order Value',
        breadcrumb: [
          {
            text: 'Average Order Value',
            active: true,
          },
        ],
      },
    },
    {
      path: '/consultation',
      name: 'consultation',
      component: () => import('@/views/Consultation.vue'),
      meta: {
        pageTitle: 'Consultation Report',
        breadcrumb: [
          {
            text: 'Consultation Report',
            active: true,
          },
        ],
      },
    },
    {
      path: '/conversion',
      name: 'conversion',
      component: () => import('@/views/Conversion.vue'),
      meta: {
        pageTitle: 'Conversion Rate',
        breadcrumb: [
          {
            text: 'Conversion Rate',
            active: true,
          },
        ],
      },
    },
    {
      path: '/login',
      name: 'login',
      component: () => import('@/views/Login.vue'),
      meta: {
        layout: 'full',
      },
    },
    {
      path: '/error-404',
      name: 'error-404',
      component: () => import('@/views/error/Error404.vue'),
      meta: {
        layout: 'full',
      },
    },
    {
      path: '*',
      redirect: 'error-404',
    },
  ],
})

function isAuthenticatedPage(route) {
  return route.name === 'login' || route.name === 'register'
}

router.beforeEach((to, _, next) => {
  if (!isUserLoggedIn() && !isAuthenticatedPage(to))
    return next({ name: 'login' })

  return next()
})

export default router
