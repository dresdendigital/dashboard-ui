import axios from '@/libs/axios'

export default {
  getCountryTags() {
    return axios.post(`vend/configs`)
  },
  getProductTags(country) {
    return axios.post(`vend/product-tags`, null, {
      params: { country_tag: country },
    })
  },
  getAOV({ group, start_date, end_date, country_tag, product_tag }) {
    return axios.post(`aov/${group}`, null, {
      params: {
        start_date,
        end_date,
        country_tag,
        product_tag,
      },
    })
  },
  getConsultation({ start_date, end_date, country_tag, product_tag }) {
    return axios.post(`consultation/report`, null, {
      params: {
        start_date,
        end_date,
        country_tag,
        product_tag,
      },
    })
  },
  getConversion({ group, start_date, end_date, country_tag, product_tag }) {
    return axios.post(`conversion/${group}`, null, {
      params: {
        start_date,
        end_date,
        country_tag,
        product_tag,
      },
    })
  },
}
