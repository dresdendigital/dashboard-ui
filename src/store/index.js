import Vue from 'vue'
import Vuex from 'vuex'
import createPersistedState from 'vuex-persistedstate'
import api from '@/services/api'
// Modules
import app from './app'
import appConfig from './app-config'
import verticalMenu from './vertical-menu'

Vue.use(Vuex)

export default new Vuex.Store({
  plugins: [
    createPersistedState({
      paths: ['user', 'countries'],
    }),
  ],
  data: {
    user: null,
    countries: [],
  },
  mutations: {
    SET_USER(state, val) {
      state.user = val
    },
    SET_COUNTRIES(state, val) {
      state.countries = val
    },
  },
  actions: {
    async getCountries({ commit }) {
      const response = await api.getCountryTags()
      commit('SET_COUNTRIES', response.data)
    },
  },
  modules: {
    app,
    appConfig,
    verticalMenu,
  },
  strict: process.env.DEV,
})
