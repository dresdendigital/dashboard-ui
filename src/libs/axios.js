import Vue from 'vue'
import axios from 'axios'

const axiosIns = axios.create({
  baseURL: 'https://dashboard-api.dresden.io/api/',
  // timeout: 1000,
  // headers: {'X-Custom-Header': 'foobar'}
})

Vue.prototype.$http = axiosIns

export default axiosIns
