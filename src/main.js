import Vue from 'vue'
import BootstrapVue from 'bootstrap-vue'
import VueCompositionAPI from '@vue/composition-api'
import dayjs from 'dayjs'
import VueGoodTable from 'vue-good-table'
import 'vue-good-table/dist/vue-good-table.css'
import vSelect from 'vue-select'
import 'vue-select/dist/vue-select.css'
import router from './router'
import store from './store'
import App from './App.vue'

// Global Components
import './global-components'

// 3rd party plugins
import '@/libs/portal-vue'
import '@/libs/toastification'

Vue.prototype.$dayjs = dayjs
// BSV Plugin Registration
Vue.use(BootstrapVue)
Vue.use(VueGoodTable)
Vue.component('v-select', vSelect)

// Composition API
Vue.use(VueCompositionAPI)

// import core styles
require('@core/scss/core.scss')

// import assets styles
require('@/assets/scss/style.scss')

Vue.config.productionTip = false

new Vue({
  router,
  store,
  render: h => h(App),
}).$mount('#app')
